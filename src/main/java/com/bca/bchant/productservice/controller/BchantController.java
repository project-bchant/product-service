package com.bca.bchant.productservice.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bca.bchant.productservice.model.ErrorSchema;
import com.bca.bchant.productservice.model.Product;
import com.bca.bchant.productservice.service.ProductService;

@CrossOrigin(origins = "*")
@RestController
public class BchantController {
	private static final Logger LOGGER = LoggerFactory.getLogger(BchantController.class);
	
	@Autowired
	ProductService productService;
	
	@RequestMapping(value = "/getAllProducts", method = RequestMethod.GET)
    public ResponseEntity<List<Product>> getAllProducts(@RequestHeader("MerchantID") String merchantId) {
		LOGGER.info("Memanggil service getAllProducts ... ");
		List<Product> listProduct = this.productService.getAllProducts(merchantId);
		
		return new ResponseEntity<List<Product>>(listProduct, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/getAllProductsForReport", method = RequestMethod.GET)
    public ResponseEntity<List<Product>> getAllProductsForReport(@RequestHeader("MerchantID") String merchantId) {
		LOGGER.info("Memanggil service getAllProductsForReport ... ");
		List<Product> listProduct = this.productService.getAllProductsForReport(merchantId);
		
		return new ResponseEntity<List<Product>>(listProduct, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/getProductById", method = RequestMethod.GET)
    public ResponseEntity<List<Product>> getProductById(@RequestHeader("MerchantID") String merchantId, @RequestHeader("ProductID") String productId) {
		LOGGER.info("Memanggil service getProductById ... ");
		List<Product> listProduct = this.productService.getProductById(merchantId, productId);
		
		return new ResponseEntity<List<Product>>(listProduct, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/addProduct", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ErrorSchema> addProduct(@RequestBody Product product) {
    	LOGGER.info("Memanggil service addProduct ...");
    	ErrorSchema errorSchema = this.productService.addProduct(product);
        
		return new ResponseEntity<ErrorSchema>(errorSchema, HttpStatus.CREATED);
    }
	
	@RequestMapping(value = "/deleteProduct", method = RequestMethod.PUT)
    public ResponseEntity<ErrorSchema> deleteProduct(@RequestHeader("ProductID") String productId) {
    	LOGGER.info("Memanggil service deleteProduct ...");
    	ErrorSchema errorSchema = this.productService.deleteProductById(productId);
    	
		return new ResponseEntity<ErrorSchema>(errorSchema, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/updateProduct", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ErrorSchema> updateProduct(@RequestBody Product product) {
    	LOGGER.info("Memanggil service updateProduct ...");
    	ErrorSchema errorSchema = this.productService.updateProduct(product);
    	
		return new ResponseEntity<ErrorSchema>(errorSchema, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/restoreProduct", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ErrorSchema> restoreProduct(@RequestBody Product product) {
    	LOGGER.info("Memanggil service restoreProduct ...");
    	ErrorSchema errorSchema = this.productService.restoreProduct(product);
    	
		return new ResponseEntity<ErrorSchema>(errorSchema, HttpStatus.OK);
    }
	
	@RequestMapping(value = "/testSelect", method = RequestMethod.GET)
    public ResponseEntity<List<Product>> testSelect() {
		LOGGER.info("Memanggil service testSelect ... ");
		List<Product> listProduct = this.productService.testSelect();
		
		return new ResponseEntity<List<Product>>(listProduct, HttpStatus.OK);
    }
}
