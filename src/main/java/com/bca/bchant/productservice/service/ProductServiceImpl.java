package com.bca.bchant.productservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bca.bchant.productservice.dao.ProductDao;
import com.bca.bchant.productservice.model.ErrorSchema;
import com.bca.bchant.productservice.model.Product;

@Service
public class ProductServiceImpl implements ProductService {
	@Autowired
	ProductDao productDao;
	
	@Override
	public List<Product> getAllProducts(String merchantId) {
		// TODO Auto-generated method stub
		return this.productDao.getAllProducts(merchantId);
	}
	
	@Override
	public List<Product> getAllProductsForReport(String merchantId) {
		// TODO Auto-generated method stub
		return this.productDao.getAllProductsForReport(merchantId);
	}
	
	@Override
	public List<Product> getProductById(String merchantId, String productId) {
		// TODO Auto-generated method stub
		return this.productDao.getProductById(merchantId, productId);
	}
	
	@Override
	public ErrorSchema addProduct(Product product) {
		// TODO Auto-generated method stub
		return this.productDao.addProduct(product);
	}
	
	@Override
	public ErrorSchema deleteProductById(String productId) {
		// TODO Auto-generated method stub
		return this.productDao.deleteProductById(productId);
	}
	
	@Override
	public ErrorSchema updateProduct(Product product) {
		// TODO Auto-generated method stub
		return this.productDao.updateProduct(product);
	}
	
	@Override
	public ErrorSchema restoreProduct(Product product) {
		// TODO Auto-generated method stub
		return this.productDao.restoreProduct(product);
	}

	@Override
	public List<Product> testSelect() {
		// TODO Auto-generated method stub
		return this.productDao.testSelect();
	}
}
