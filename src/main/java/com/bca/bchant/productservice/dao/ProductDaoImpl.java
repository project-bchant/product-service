package com.bca.bchant.productservice.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.bca.bchant.productservice.dbconn.getDbConnection;
import com.bca.bchant.productservice.model.ErrorSchema;
import com.bca.bchant.productservice.model.Product;

import oracle.jdbc.OracleTypes;

@Repository
public class ProductDaoImpl implements ProductDao {
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductDaoImpl.class);
	
	Connection conn = null;
	PreparedStatement pSt = null;
	CallableStatement callableStatement = null;
	ResultSet rs = null;
	
	@Value("${product.queryGetAllProducts}")
	String queryGetAllProducts;

	@Value("${product.queryGetAllProductsForReport}")
	String queryGetAllProductsForReport;
	
	@Value("${product.queryGetProductById}")
	String queryGetProductById;
	
	@Value("${product.queryDeleteProduct}")
	String queryDeleteProduct;
	
	@Value("${product.queryTest}")
	String queryTest;
	
	
	@Override
	public List<Product> getAllProducts(String merchantId) {		
/*		String query = "SELECT productid, nama, qty, harga, kode, merchantid "
				+ "FROM product WHERE merchantid = ? "
				+ "ORDER BY productid ASC";*/
		Product product = new Product();
		List<Product> listProduct = new ArrayList<Product>();
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + queryGetAllProducts + " ...");
			
			pSt = conn.prepareStatement(queryGetAllProducts);
			LOGGER.info("Prepared Statement dengan isi " + queryGetAllProducts + " berhasil dijalanakan ...");
			
			pSt.setString(1, merchantId);
			
			rs = pSt.executeQuery();
			
			if(!rs.isBeforeFirst()) {

			}
			else {
				while(rs.next()){
					product = new Product();
					
					product.setProductId(rs.getString("productid"));
					product.setNama(rs.getString("nama"));
					product.setStok(rs.getString("qty"));
					product.setHarga(rs.getString("harga"));
					product.setKode(rs.getString("kode"));
					product.setMerchantId(rs.getString("merchantid"));
					listProduct.add(product);
				}
				LOGGER.info("Berhasil mendapatkan data produk ...");
			}
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return listProduct;
	}
	
	@Override
	public List<Product> getAllProductsForReport(String merchantId) {		
/*		String query = "SELECT productid, nama, qty, harga, kode, merchantid "
				+ "FROM product WHERE merchantid = ? "
				+ "ORDER BY productid ASC";*/
		Product product = new Product();
		List<Product> listProduct = new ArrayList<Product>();
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + queryGetAllProductsForReport + " ...");
			
			pSt = conn.prepareStatement(queryGetAllProductsForReport);
			LOGGER.info("Prepared Statement dengan isi " + queryGetAllProductsForReport + " berhasil dijalanakan ...");
			
			pSt.setString(1, merchantId);
			
			rs = pSt.executeQuery();
			
			if(!rs.isBeforeFirst()) {

			}
			else {
				while(rs.next()){
					product = new Product();
					
					product.setProductId(rs.getString("productid"));
					product.setNama(rs.getString("nama"));
					product.setStok(rs.getString("qty"));
					product.setHarga(rs.getString("harga"));
					product.setKode(rs.getString("kode"));
					product.setMerchantId(rs.getString("merchantid"));
					listProduct.add(product);
				}
				LOGGER.info("Berhasil mendapatkan data produk ...");
			}
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return listProduct;
	}

	@Override
	public List<Product> getProductById(String merchantId, String productId) {
		/*String query = "SELECT productid, nama, qty, harga, kode, merchantid "
				+ "FROM product WHERE merchantid = ? AND productid = ? "
				+ "ORDER BY productid ASC";*/
		
		List<Product> listProduct = new ArrayList<Product>();
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + queryGetProductById + " ...");
			
			pSt = conn.prepareStatement(queryGetProductById);
			LOGGER.info("Prepared Statement dengan isi " + queryGetProductById + " berhasil dijalanakan ...");
			
			pSt.setString(1, merchantId);
			pSt.setString(2, productId);
			
			rs = pSt.executeQuery();
	
			while(rs.next()){
				Product product = new Product();
				
				product.setProductId(rs.getString("productid"));
				product.setNama(rs.getString("nama"));
				product.setStok(rs.getString("qty"));
				product.setHarga(rs.getString("harga"));
				product.setKode(rs.getString("kode"));
				product.setMerchantId(rs.getString("merchantid"));
				listProduct.add(product);
			}
			LOGGER.info("Berhasil mendapatkan data produk dengan ID produk " + productId);
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return listProduct;
	}
	
	@Override
	public ErrorSchema addProduct(Product product) {
		ErrorSchema errorSchema = new ErrorSchema();
		String errorMessage = "";
		String errorMessageKode = "";
		Integer errorMessageNumb = 0;
		String spName = "INSERT_PRODUCT";
		
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//StoredProcedure
			LOGGER.info("Siap menjalankan stored procedure " + spName + " ...");
			
			callableStatement = conn.prepareCall("{call " + spName + "(?,?,?,?,?,?,?)}");
			LOGGER.info("Stored Procedure dengan nama " + spName + " berhasil dijalanakan ...");

			String query = "SELECT productid+1 as \"productid\"" +
							" FROM product" +
							" WHERE ROWNUM <= 1" +
							" ORDER BY productid DESC";
			pSt = conn.prepareStatement(query);
			rs = pSt.executeQuery();
			while(rs.next()){
				LOGGER.info("productid => " + rs.getString("productid"));
				callableStatement.setInt(1, Integer.parseInt(rs.getString("productid")));
				callableStatement.setString(2, product.getNama());
				callableStatement.setString(3, product.getStok());
				callableStatement.setString(4, product.getHarga());
				callableStatement.setString(5, product.getKode());
				callableStatement.setString(6, product.getMerchantId());
				callableStatement.registerOutParameter(7, OracleTypes.CURSOR);
			}
			
			callableStatement.executeUpdate();
			LOGGER.info("Mengeksekusi stored procedure dengan nama " + spName + " ...");
	
			rs = (ResultSet) callableStatement.getObject(7);
			while(rs.next()){
				errorMessageNumb = rs.getInt("v_error_msg_numb");
				errorMessage = rs.getString("v_error_msg");
				errorMessageKode = rs.getString("v_error_msg_kode");
			}

			callableStatement.executeUpdate();
			if(errorMessageNumb == 1) {
				errorSchema.setErrorCode("ESB-00-00");
				errorSchema.setErrorMessage(errorMessage);
				LOGGER.info("Berhasil melakukan insert produk dengan id produk " + product.getProductId() + " ...");
			}
			else {
				errorSchema.setErrorCode("ESB-99-999");
				errorSchema.setErrorMessage(errorMessage);
				errorSchema.setProductCode(errorMessageKode);
				LOGGER.error("Gagal melakukan insert, coba cek ProductDaoImpl addProduct ...");
			}
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return errorSchema;
	}
	
	@Override
	public ErrorSchema deleteProductById(String productId) {
		Integer resultdelete = 0;
		ErrorSchema errorSchema = new ErrorSchema();
		
		/*String queryDel =	"DELETE FROM product "
	     + "WHERE productid = ?";*/
		
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			LOGGER.info("Siap menjalankan query " + queryDeleteProduct + " ...");
			
			pSt = conn.prepareStatement(queryDeleteProduct);
			LOGGER.info("Prepared Statement dengan isi " + queryDeleteProduct + " berhasil dijalanakan ...");
			
			pSt.setString(1, productId);
			
			resultdelete = pSt.executeUpdate();
			
			if(resultdelete == 0) {
				errorSchema.setErrorCode("ESB-99-999");
				errorSchema.setErrorMessage("Delete Gagal, ID produk tidak ditemukan");
				LOGGER.error("Delete gagal, ID produk tidak ditemukan ...");
			}
			else if(resultdelete == 1) {
				errorSchema.setErrorCode("ESB-00-000");
				errorSchema.setErrorMessage("Produk berhasil dihapus");
				LOGGER.info("Delete berhasil ...");
			}
			
		} catch(Exception e) {
			System.out.println("error -> " +  e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return errorSchema;
	}

	@Override
	public ErrorSchema updateProduct(Product product) {
		ErrorSchema errorSchema = new ErrorSchema();
		String errorMessage = "";
		Integer errorMessageNumb = 0;
		String spName = "UPDATE_PRODUCT";
		
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//StoredProcedure
			LOGGER.info("Siap menjalankan stored procedure " + spName + " ...");
			
			callableStatement = conn.prepareCall("{call " + spName + "(?,?,?,?,?,?,?)}");
			LOGGER.info("Stored Procedure dengan nama " + spName + " berhasil dijalanakan ...");

			callableStatement.setString(1, product.getProductId());
			callableStatement.setString(2, product.getNama());
			callableStatement.setString(3, product.getStok());
			callableStatement.setString(4, product.getHarga());
			callableStatement.setString(5, product.getKode());
			callableStatement.setString(6, product.getMerchantId());
			callableStatement.registerOutParameter(7, OracleTypes.CURSOR);
			LOGGER.info("Menjalankan registerOutParameter untuk Cursor dengan value -> " + OracleTypes.CURSOR + " ...");
			
			callableStatement.executeUpdate();
			LOGGER.info("Mengeksekusi stored procedure dengan nama " + spName + " ...");
	
			rs = (ResultSet) callableStatement.getObject(7);
			while(rs.next()){
				errorMessageNumb = rs.getInt("v_error_msg_numb");
				errorMessage = rs.getString("v_error_msg");
			}

			callableStatement.executeUpdate();
			if(errorMessageNumb == 1) {
				errorSchema.setErrorCode("ESB-00-00");
				errorSchema.setErrorMessage(errorMessage);
				LOGGER.info("Berhasil melakukan update produk dengan id produk " + product.getProductId() + " ...");
			}
			else {
				errorSchema.setErrorCode("ESB-99-999");
				errorSchema.setErrorMessage(errorMessage);
				LOGGER.error("Gagal melakukan update, coba cek ProductDaoImpl updateProduct ...");
			}
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return errorSchema;
	}

	@Override
	public ErrorSchema restoreProduct(Product product) {
		ErrorSchema errorSchema = new ErrorSchema();
		String errorMessage = "";
		Integer errorMessageNumb = 0;
		String spName = "RESTORE_PRODUCT";
		
		try{//DBUtils db = new DBUtils(DbAccessConfig.getJNDIName());
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//StoredProcedure
			LOGGER.info("Siap menjalankan stored procedure " + spName + " ...");
			
			callableStatement = conn.prepareCall("{call " + spName + "(?,?,?)}");
			LOGGER.info("Stored Procedure dengan nama " + spName + " berhasil dijalanakan ...");

			callableStatement.setString(1, product.getKode());
			callableStatement.setString(2, product.getMerchantId());
			callableStatement.registerOutParameter(3, OracleTypes.CURSOR);
			LOGGER.info("Menjalankan registerOutParameter untuk Cursor dengan value -> " + OracleTypes.CURSOR + " ...");
			
			callableStatement.executeUpdate();
			LOGGER.info("Mengeksekusi stored procedure dengan nama " + spName + " ...");
	
			rs = (ResultSet) callableStatement.getObject(3);
			while(rs.next()){
				errorMessageNumb = rs.getInt("v_error_msg_numb");
				errorMessage = rs.getString("v_error_msg");
			}

			callableStatement.executeUpdate();
			if(errorMessageNumb == 1) {
				errorSchema.setErrorCode("ESB-00-00");
				errorSchema.setErrorMessage(errorMessage);
				LOGGER.info("Berhasil mengembalikan produk dengan kode produk " + product.getKode() + " ...");
			}
			else {
				errorSchema.setErrorCode("ESB-99-999");
				errorSchema.setErrorMessage(errorMessage);
				LOGGER.error("Gagal melakukan update, coba cek ProductDaoImpl restoreProduct ...");
			}
		} catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return errorSchema;
	}

	@Override
	public List<Product> testSelect() {
		
		List<Product> listProduct = new ArrayList<Product>();
		Product product = new Product();
		try{
			conn = getDbConnection.Access(); //Get Connection
			LOGGER.info("Membuka koneksi database ...");
			
			//PreparedStatement
			
			pSt = conn.prepareStatement(queryTest);
			LOGGER.info("Prepared Statement dengan isi " + queryTest + " berhasil dijalanakan ...");
			
			rs = pSt.executeQuery();
			
			if(!rs.isBeforeFirst()) {
				product = new Product();
				product.setNama("Data Kosong");
			}
			else {
				while(rs.next()) {
					product = new Product();
					product.setNama(rs.getString("nama"));
					product.setProductId(rs.getString("productid"));
					product.setStok(rs.getString("qty"));
					product.setHarga(rs.getString("harga"));
					product.setKode(rs.getString("kode"));
					product.setMerchantId(rs.getString("merchantid"));
					listProduct.add(product);
				}
			}
			
		} catch(Exception e) {
			System.out.println("error -> " +  e.getMessage());
			e.printStackTrace();
		} finally {
			try{
				if (rs != null) {
					rs.close();
				}

				if (callableStatement != null) {
					callableStatement.close();
				}

				if (conn != null) {
					conn.close();
				}
				
				if (pSt != null) {
					pSt.close();
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		
		return listProduct;
	}
}
