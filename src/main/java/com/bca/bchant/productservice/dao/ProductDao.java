package com.bca.bchant.productservice.dao;

import java.util.List;

import com.bca.bchant.productservice.model.ErrorSchema;
import com.bca.bchant.productservice.model.Product;

public interface ProductDao {
	List<Product> getAllProducts(String merchantId);
	List<Product> getAllProductsForReport(String merchantId);
	List<Product> getProductById(String merchantId, String productId);
	ErrorSchema addProduct(Product product);
	ErrorSchema deleteProductById(String productId);
	ErrorSchema updateProduct(Product product);
	ErrorSchema restoreProduct(Product product);
	List<Product> testSelect();
}
